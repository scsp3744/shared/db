const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    user_role_id: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    official_email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    last_login: {
        type: Date,
        required: true
    }
});

const userRoleSchema = new mongoose.Schema({
    role: {
        type: String,
        required: true
    }
});

mongoose.model('User', userSchema, 'User');
mongoose.model('UserRole', userRoleSchema, 'UserRole');