const mongoose = require('mongoose');

// const disabilityStatusSchema = new mongoose.Schema({
//     status: {
//         type: String,
//         required: true
//     }
// });

const employeeStatusSchema = new mongoose.Schema({
    status: {
        type: String,
        required: true
    }
});

const localStateSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
});

const marriageStatusSchema = new mongoose.Schema({
    status: {
        type: String,
        required: true
    }
});

const nationSchema = new mongoose.Schema({
    nation: {
        type: String,
        required: true
    }
});

const raceSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
});

const religionSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
});

const genderSchema = new mongoose.Schema({
    gender: {
        type: String,
        required: true
    }
});

// mongoose.model('DisabilityStatus', disabilityStatusSchema);
mongoose.model('Religion', religionSchema, 'Religion');
mongoose.model('EmployeeStatus', employeeStatusSchema, 'EmployeeStatus');
mongoose.model('LocalState', localStateSchema, 'LocalState');
mongoose.model('MarriageStatus', marriageStatusSchema, 'MarriageStatus');
mongoose.model('Nation', nationSchema, 'Nation');
mongoose.model('Race', raceSchema, 'Race');
mongoose.model('Gender', genderSchema, 'Gender');