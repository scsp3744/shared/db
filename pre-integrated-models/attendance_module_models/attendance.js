const mongoose = require('mongoose');

const Attendance = mongoose.model('Attendance', {
  Lecturer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Lecturer',
    required: true
  },
  CurrentCourse: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'CurrentCourse',
    required: true
  },
  TimeStart: { type: Number, required: true },
  TimeStop: { type: Number, required: true },
  key: { type: String, required: true }
});

const StudentAttendance = mongoose.model('StudentAttendance', {
  Attendance: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Attendance',
    required: true
  },
  Student: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Student',
    required: true
  },
  TimeRegister: { type: Number, required: true },
  Attended: { type: String, required: true }
});

// demo student model from other group
const Student = mongoose.model('Student', {
  Name: { type: String, required: true },
  MatricNo: { type: String, required: true }
});

// demo course model from other group
const CurrentCourse = mongoose.model('CurrentCourse', {
  Name: { type: String, required: true },
  Code: { type: String, required: true },
  Credit: { type: Number, required: true },
  NumberOfStudent: { type: Number, required: true },
  Section: { type: Number, required: true },
  Lecturer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Lecturer',
    required: true
  }
});

// demo lecturer model from other group
const Lecturer = mongoose.model('Lecturer', {
  Name: { type: String, required: true },
});
const StudentCourse = mongoose.model('StudentCourse', {
  CurrentCourse: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'CurrentCourse',
    required: true
  },
  Student: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Student',
    required: true
  }
});

const RequestAbsent = mongoose.model('RequestAbsent', {
  Student: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Student',
    required: true
  },
  CurrentCourse: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'CurrentCourse',
    required: true
  },
  Reason: { type: String, required: true },
  CreatedOn: { type: Number, required: true },
  RequestOn: { type: Number, required: true }
});

var LectAttendance = mongoose.model('LectAttendance', {
  Lecturer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Lecturer',
    required: true
  },
  Status: String,
  checkin_date: Date,
  checkin_time: Number,
  checkout_time: Number,

});


module.exports = {
  Attendance: Attendance,
  StudentAttendance: StudentAttendance,
  Student: Student,
  CurrentCourse: CurrentCourse,
  Lecturer: Lecturer,
  StudentCourse: StudentCourse,
  RequestAbsent: RequestAbsent,
  LectAttendance: LectAttendance
};
