# Database Information
## Cloud Server
- Cloud Platform Provider: MongoDB Atlas (using)
- Provided by *Ooi Chee Seang*
- Connection String:  
`mongodb+srv://utmsmart:utmsmart@utmsmart-fnqwl.gcp.mongodb.net/test`
- Tools that can connect to and view database: Compass, Robo3T, etc. 